//
//  ContentView.swift
//  sensors
//
//  Created by Josemaria Carazo Abolafia on 3/5/21.
//

import SwiftUI

struct ContentView: View {
    
    //En Demo-0 vimos que una variable era @State y así su valor se actualizaba allí donde estuviera la variable, cada vez que la variable cambiara de valor.
    //Aquí no se trata de una variable, sino de una clase que hemos creado en otro archivo -sensunit.swift
    //Para que sus propiedades se actualicen en la pantalla...
    //1-. La clase tiene que ser ObservableObject
    //2-. Las variables que queremos en tiempo real han de tener el atributo @Published
    //3-. La instancia que declaramos aquí ha de tener el atributo @ObservedObject:
    // +info: sensunit.swift
    
    @ObservedObject var sensor: sensunit = sensunit()

    init() {
        sensor.start()
    }
    
    var body: some View {
        Section {
            Label("SwiftUI iSensors", systemImage: "iphone")
                .font(/*@START_MENU_TOKEN@*/.headline/*@END_MENU_TOKEN@*/)
            Label("by LDTS", systemImage: "signature")
                .font(.subheadline).padding()
        }
        
        
        Divider().padding(.top, 10.0).padding(.bottom, 20.0)


        LazyVStack {
            //La siguiente función de sensor utiliza las variables Published. Dado que Sensor es un ObservedObject, este texto se actualiza cada vez que esas variables cambian.
            Text(sensor.monitoring())
                .foregroundColor(Color.yellow)
                .padding()
            
            Divider()
            
            //Ejemplo dummy de ForEach... para tener la sintaxis a mano
            ForEach(1...1, id: \.self) { count in
                Text("Move your iPhone!").italic()
                    .fontWeight(.light)
                    .foregroundColor(Color.red)
                    .padding()
            }
            
            Divider()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
